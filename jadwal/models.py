from django.db import models

# Create your models here.
class Sched(models.Model):
    name = models.CharField(max_length = 50)
    tempat = models.CharField(max_length = 50)
    kategori = models.CharField(max_length = 50)
    tanggal = models.DateField()
    waktu = models.TimeField()
