from django.shortcuts import render
from .forms import ScheduleForm
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Sched

# Create your views here.

def schedule(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)

        if form.is_valid():
            nama = form.cleaned_data['name']
            place = form.cleaned_data['tempat']
            date = form.cleaned_data['tanggal']
            time = form.cleaned_data['waktu']
            category = form.cleaned_data['kategori']

            jadwal = Sched(name = nama , tempat = place , tanggal = date, waktu = time,kategori = category )
            jadwal.save()

            return HttpResponseRedirect(reverse('sched:schedule'))
        else:
            schedules = Sched.objects.all()
            context = {
                'sched' : form,
                'jadwalbaru' : schedules
            }
            return render(request,'schedule.html',context)


    schedules = Sched.objects.all()
    print(schedules)
    form = ScheduleForm()
    context =  {
        'sched': form,
        'jadwalbaru' : schedules,
    }
    return render(request,'schedule.html',context)

def delete(request, id):
    Sched.objects.get(pk = id).delete()
    return HttpResponseRedirect(reverse('sched:schedule'))
