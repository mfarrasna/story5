from django import forms

class ScheduleForm(forms.Form):
    name = forms.CharField(max_length = 50)
    tempat = forms.CharField(max_length = 50)
    tanggal = forms.DateField()
    waktu = forms.TimeField()
    kategori = forms.CharField(max_length = 50)

