from django.urls import path
from . import views

app_name = 'sched'

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('delete/<int:id>', views.delete, name='deletesched')
    # dilanjutkan ...
]